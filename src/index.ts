import express, { Request, Response, NextFunction } from "express";
import cors from "cors";
import mongoose from "mongoose";

import adminRouter from "./routes/adminRoute";
import busRouter from "./routes/busRoute";
import bookingRouter from "./routes/bookingRoute";
import routeRouter from "./routes/routeRoute";
require("dotenv").config();

//setting up express app
let app = express();
app.use(cors({}));
app.use(
  express.urlencoded({
    extended: true,
  }),
);
app.use(express.json());

//Connecting with mongoose
mongoose
  .connect(process.env.MONGO_CON_STRING, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true })
  .then(() => {
    console.log("Mongoose connected");
  })
  .catch((err) => {
    throw err;
  });
var db = mongoose.connection;
db.on("error", console.error.bind(console, "MongoDB connection error:"));

// Creating routes
app.use("/admin", adminRouter);
app.use("/bus", busRouter);
app.use("/booking", bookingRouter);
app.use("/route", routeRouter);
app.get("/", (req: Request, response: Response, next: NextFunction) => response.json("hello"));

// Error handling
const errorHandler = (err: Error | undefined, request: Request, response: Response, next: NextFunction) => {
  console.log("using error handler");
  return response.status(500).json({ err: err?.message });
};
app.use(errorHandler);

// Starting express server
const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
  console.log(`Server started listening on port ${PORT}`);
});
