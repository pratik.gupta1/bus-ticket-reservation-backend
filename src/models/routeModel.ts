import mongoose, { Document } from "mongoose";
import { IBus } from "./busModel";
export interface IRoute extends Document {
  from: string;
  to: string;
  bus: IBus["_id"];
  days: number[];
  departureHour: number;
  departureMinute: number;
  travelDuration: number;
  price: number;
}

export interface IRouteModel extends mongoose.Model<IRoute> {}

const routeSchema = new mongoose.Schema<IRoute>({
  from: { type: String, required: true, set: (v: string) => v.toLowerCase(), get: (v: string) => v.toLowerCase() },
  to: { type: String, required: true, set: (v: string) => v.toLowerCase(), get: (v: string) => v.toLowerCase() },
  bus: { type: mongoose.Schema.Types.ObjectId, ref: "Bus", required: true },
  days: [Number], //1=Sunday, 7=Saturday
  departureHour: { type: Number, required: true },
  departureMinute: { type: Number, required: true },
  travelDuration: { type: Number, required: true },
  price: { type: Number, required: true },
});

const routeModel = mongoose.model<IRoute, IRouteModel>("Route", routeSchema);
export default routeModel;
