import mongoose, { Document } from "mongoose";

export interface IBus extends Document {
  vehicleNumber: string;
  numOfSeats: number;
  vehicleMake: string;
  vehicleModel: string;
  images: string[];
  busType: string;
}

export interface IBusModel extends mongoose.Model<IBus> {}

const BUS_TYPES = ["Sitter", "Sleeper", "Semi sleeper"];

export const busSchema = new mongoose.Schema<IBus>({
  vehicleNumber: { type: String, required: true, unique: true },
  numOfSeats: { type: Number, required: true },
  vehicleMake: { type: String, required: true },
  vehicleModel: { type: String, required: true },
  images: [{ type: String }],
  busType: { type: String, enum: BUS_TYPES, default: BUS_TYPES[0] },
});

const BusModel = mongoose.model<IBus, IBusModel>("Bus", busSchema);

export default BusModel;
