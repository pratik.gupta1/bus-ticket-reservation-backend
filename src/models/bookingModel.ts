import mongoose, { Document } from "mongoose";
import { IRoute } from "./routeModel";

interface IPersonDetails extends Document {
  name: string;
  mobile: string;
  email: string;
  age?: number;
  gender?: "M" | "F";
}

// const personDetailsSchema = new mongoose.Schema<IPersonDetails>({
//   name: { type: String, required: true },
//   mobile: { type: String, required: true },
//   email: { type: String, required: true },
//   age: { type: Number },
//   gender: { type: String, enum: ["M", "F"] },
// });

export interface IBooking extends Document {
  route: IRoute["_id"];
  travelDate: Date;
  reservations: { personDetails: IPersonDetails; seatNumber: number }[];
}

export interface IBookingModel extends mongoose.Model<IBooking> {}

const bookingSchema = new mongoose.Schema<IBooking>({
  route: { type: mongoose.Schema.Types.ObjectId, ref: "Route", required: true },
  travelDate: { type: Date, required: true },
  reservations: [
    {
      type: new mongoose.Schema(
        {
          personDetails: {
            name: { type: String, required: true },
            mobile: { type: String, required: true },
            email: { type: String, required: true },
            age: { type: Number },
            gender: { type: String, enum: ["M", "F"] },
          },
          seatNumber: { type: Number, required: true },
        },
        { timestamps: true },
      ),
    },
  ],
});

const bookingModel = mongoose.model<IBooking, IBookingModel>("Booking", bookingSchema);
export default bookingModel;
