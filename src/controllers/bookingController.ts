import { Request, Response, NextFunction, request } from "express";
import { body } from "express-validator";
import BookingModel from "../models/bookingModel";
import RouteModel from "../models/routeModel";

const validations = {
  postBooking: [
    body("personDetails")
      .exists()
      .custom((value) => {
        if (value["name"] && value["email"] && value["mobile"]) {
          return true;
        }
        throw new Error("name, email, mobile are required in person details");
      }),
    body("route").exists(),
    body("seatNumber").isNumeric(),
    body("travelDate").isDate(),
  ],
};

const postBooking = async (request: Request, response: Response, next: NextFunction) => {
  const { travelDate, route, seatNumber, personDetails } = request.body;
  try {
    const routeDoc = await RouteModel.findById(route).populate("bus");
    if (routeDoc === null) {
      return response.status(404).json({ msg: `Route with id ${route} doesn't exists` });
    }
    let weekDay = new Date(travelDate).getDay() + 1;
    if (!routeDoc.days.includes(weekDay)) {
      return response.status(400).json({ msg: `This bus doesn't run on ${travelDate}` });
    }
    if (routeDoc.bus.numOfSeats < seatNumber) {
      return response
        .status(403)
        .json({ msg: `seat number should be less than numOfseats in bus i.e. ${routeDoc.bus.numOfSeats}` });
    }
    const oldBooking = await BookingModel.findOne({
      route: route,
      travelDate: travelDate,
      "reservations.seatNumber": seatNumber,
    });
    if (oldBooking !== null) {
      return response.status(403).json({ msg: `Seat number ${seatNumber} is already booked` });
    }
    const booking = await BookingModel.findOneAndUpdate(
      { travelDate, route },
      {
        $push: {
          reservations: {
            seatNumber: seatNumber,
            personDetails: personDetails,
          },
        },
      },
      { new: true, upsert: true },
    );
    // const newBooking = new BookingModel({ ...request.body });
    // await newBooking.save();
    return response.json(booking.reservations[booking.reservations.length - 1]);
  } catch (error) {
    next(error);
  }
};

const getBooking = async (request: Request, response: Response, next: NextFunction) => {
  const { bookingId } = request.params;
  try {
    const allBookings = await BookingModel.findOne({ "reservations._id": bookingId });
    const booking = (allBookings?.reservations as any).id(bookingId); // TODO type info lost as couldn't find id method
    return response.json(booking);
  } catch (error) {
    next(error);
  }
};

const deleteBooking = async (request: Request, response: Response, next: NextFunction) => {
  const { bookingId } = request.params;
  try {
    const allBookings = await BookingModel.findOne({ "reservations._id": bookingId });
    if (allBookings === null) {
      return response.status(404).json({ msg: `Booking with id ${bookingId} not found` });
    }
    await (allBookings?.reservations as any).pull(bookingId);
    await allBookings?.save();
    return response.json({ msg: `success` });
  } catch (error) {
    next(error);
  }
};

export default {
  validations,
  postBooking,
  getBooking,
  deleteBooking,
};
