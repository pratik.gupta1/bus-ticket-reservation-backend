import { Request, Response, NextFunction } from "express";
import { body } from "express-validator";
import { report } from "process";
import BusModel from "../models/busModel";
import RouteModel from "../models/routeModel";
import BookingModel from "../models/bookingModel";

const validations = {
  postBus: [body(["vehicleNumber", "vehicleMake", "vehicleModel"]).exists(), body("numOfSeats").exists().isNumeric()],
  putBus: [
    body("vehicleNumber").custom((value) => {
      if (value) {
        //   throw Error("vehicleNumber can't be updated"); // TODO uncomment if vehicleNumber can't be updated
        return true;
      }
      return true;
    }),
  ],
};

const getAllBuses = async (request: Request, response: Response, next: NextFunction) => {
  try {
    const allBuses = (await BusModel.find({})).reverse();
    return response.json(allBuses);
  } catch (error) {
    next(error);
  }
};

const postBus = async (request: Request, response: Response, next: NextFunction) => {
  const {
    vehicleNumber,
    numOfSeats,
    vehicleMake,
    vehicleModel,
  }: { vehicleNumber: string; numOfSeats: number; vehicleMake: string; vehicleModel: string } = request.body;
  try {
    const oldBus = await BusModel.findOne({ vehicleNumber: vehicleNumber });
    if (oldBus !== null) {
      return response.status(400).json({ msg: `Bus with vehicleNumber ${vehicleNumber} already exists` });
    }
    const newBus = new BusModel({ ...request.body });
    // const newBus = new BusModel({ vehicleNumber, vehicleMake, vehicleModel, numOfSeats });
    await newBus.save();
    return response.json(newBus);
  } catch (error) {
    if (error.code === 11000) {
      return response
        .status(400)
        .json({ msg: `Bus with vehicleNumber ${vehicleNumber} already exists. You might wanna send a PUT request` });
    }
    next(error);
  }
};

const getBus = async (request: Request, response: Response, next: NextFunction) => {
  const { busId } = request.params; //  doesn't work- const { busId } : { busId : string } = request.params;
  try {
    const bus = await BusModel.findById(busId);
    if (bus === null) {
      return response.status(404).json({ msg: `Bus with id ${busId} not found` });
    } else {
      return response.json(bus);
    }
  } catch (error) {
    next(error);
  }
};

const deleteBus = async (request: Request, response: Response, next: NextFunction) => {
  const { busId } = request.params;
  try {
    let res = await BusModel.deleteOne({ _id: busId });
    const routes = await RouteModel.find({ bus: busId });
    const routeIds = routes.map((route) => route._id);
    // const bookings =  await BookingModel.find({"route": {$in: routeIds}});
    await BookingModel.remove({ route: { $in: routeIds } });

    await RouteModel.remove({ bus: busId });
    return response.json(res);
  } catch (error) {
    next(error);
  }
};

const putBus = async (request: Request, response: Response, next: NextFunction) => {
  const { busId } = request.params;
  try {
    let updatedBus = await BusModel.findOneAndUpdate({ _id: busId }, { ...request.body }, { new: true });
    return response.json(updatedBus);
  } catch (error) {
    next(error);
  }
};

export default {
  getAllBuses,
  getBus,
  postBus,
  deleteBus,
  putBus,
  validations,
};
