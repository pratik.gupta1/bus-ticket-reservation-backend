import { Request, Response, NextFunction } from "express";
import { body, validationResult } from "express-validator";
import jwt from "jsonwebtoken";

const validations = {
  signin: [body("email").exists().isEmail(), body("password").exists()],
};

const signin = async (request: Request, response: Response, next: NextFunction) => {
  try {
    const { email, password }: { email: string; password: string } = request.body;
    if (email === process.env.EMAIL && password === process.env.PASSWORD) {
      let token = jwt.sign({ email: email }, process.env.JWT_SECRET, {
        expiresIn: 86400, // 24 hours
      });
      return response.json({ accessToken: token });
    } else {
      return response.status(403).json({ msg: "Email or password is incorrect" });
    }
  } catch (error) {
    next(error);
  }
};

export default {
  signin,
  validations,
};
