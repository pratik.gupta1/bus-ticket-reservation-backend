import { Request, Response, NextFunction } from "express";
import { body, query } from "express-validator";
import RouteModel from "../models/routeModel";
import BookingModel from "../models/bookingModel";

const validations = {
  postRoute: [
    body(["to", "from", "bus"]).exists(),
    body(["departureHour", "departureMinute", "travelDuration", "price"]).isNumeric(),
  ],
  findRoutes: [query(["from", "to"]).exists().toLowerCase(), query("travelDate").isDate()],
  findRoutesWithDetails: [query(["from", "to"]).exists().toLowerCase(), query("travelDate").isDate()],
  getAllRoutes: [query(["from", "to"]).toLowerCase()],
};

const postRoute = async (request: Request, response: Response, next: NextFunction) => {
  try {
    const newRoute = new RouteModel({ ...request.body });
    await newRoute.save();
    return response.json(newRoute);
  } catch (error) {
    next(error);
  }
};

const getAllRoutes = async (request: Request, response: Response, next: NextFunction) => {
  try {
    console.log(request.query);
    let filterQuery: { [k: string]: any } = {};
    if (request.query.days) {
      filterQuery["days"] = request.query.days;
    }
    if (request.query.from) {
      filterQuery["from"] = request.query.from;
    }
    if (request.query.to) {
      filterQuery["to"] = request.query.to;
    }
    const routes = await RouteModel.find({ ...filterQuery });
    return response.json(routes);
  } catch (error) {
    next(error);
  }
};

const findRoutes = async (request: Request, response: Response, next: NextFunction) => {
  let { from, to, travelDate } = request.query as { from: any; to: any; travelDate: any };
  travelDate = new Date(travelDate);
  const day = travelDate.getDay() + 1;
  console.log(travelDate, day);
  try {
    // const routes = await RouteModel.find({ from: from, to: to, days: day });
    // const routeIds = routes.map((doc) => doc._id);
    // const routesWithRes = await BookingModel.find({ route: { $in: routeIds }, travelDate: travelDate })
    //   .populate("route")
    //   .populate({
    //     path: "route",
    //     populate: {
    //       path: "bus",
    //       model: "Bus",
    //     },
    //   });
    // const routesWithRes = await BookingModel.aggregate([
    //   {
    //     $match: {
    //       route: { $in: routeIds },
    //       travelDate: travelDate,
    //     },
    //   },
    //   {
    //     $lookup: {
    //       from: "routes",
    //       localField: "route",
    //       foreignField: "_id",
    //       as: "route",
    //     },
    //   },
    //   {
    //     $lookup: {
    //       from: "buses",
    //       localField: "route.bus",
    //       foreignField: "_id",
    //       as: "bus",
    //     },
    //   },
    //   {
    //     $project: {
    //       route: { $arrayElemAt: ["$route", 0] },
    //       bus: { $arrayElemAt: ["$bus", 0] },
    //       reservations: 1,
    //       travelDate: 1,
    //     },
    //   },
    //   {
    //     $project: {
    //       "route.bus": 0,
    //       "reservations.personDetails": 0,
    //     },
    //   },
    // ]);
    const routesWithRes = await RouteModel.aggregate([
      {
        $match: {
          from: from,
          to: to,
          days: day,
        },
      },
      {
        $lookup: {
          from: "bookings",
          as: "bookings",
          let: { routeId: "$_id" },
          pipeline: [
            {
              $match: {
                travelDate: travelDate,
                $expr: { $eq: ["$route", "$$routeId"] },
              },
            },
          ],
        },
      },
      {
        $lookup: {
          from: "buses",
          localField: "bus",
          foreignField: "_id",
          as: "bus",
        },
      },
      {
        $addFields: {
          bus: { $arrayElemAt: ["$bus", 0] },
          reservations: {
            $cond: {
              if: { $gte: [{ $size: "$bookings" }, 1] },
              then: { $arrayElemAt: ["$bookings.reservations", 0] },
              else: [],
            },
          },
          travelDate: travelDate,
        },
      },
      {
        $project: {
          bookings: 0,
          "reservations.personDetails": 0,
        },
      },
    ]);
    return response.json(routesWithRes);
  } catch (error) {
    next(error);
  }
};

const findRoutesWithDetails = async (request: Request, response: Response, next: NextFunction) => {
  let { from, to, travelDate } = request.query as { from: any; to: any; travelDate: any };
  travelDate = new Date(travelDate);
  const day = travelDate.getDay() + 1;
  console.log(travelDate, day);
  try {
    const routesWithRes = await RouteModel.aggregate([
      {
        $match: {
          from: from,
          to: to,
          days: day,
        },
      },
      {
        $lookup: {
          from: "bookings",
          as: "bookings",
          let: { routeId: "$_id" },
          pipeline: [
            {
              $match: {
                travelDate: travelDate,
                $expr: { $eq: ["$route", "$$routeId"] },
              },
            },
          ],
        },
      },
      {
        $lookup: {
          from: "buses",
          localField: "bus",
          foreignField: "_id",
          as: "bus",
        },
      },
      {
        $addFields: {
          bus: { $arrayElemAt: ["$bus", 0] },
          reservations: {
            $cond: {
              if: { $gte: [{ $size: "$bookings" }, 1] },
              then: { $arrayElemAt: ["$bookings.reservations", 0] },
              else: [],
            },
          },
          travelDate: travelDate,
        },
      },
      {
        $project: {
          bookings: 0,
          // "reservations.personDetails": 0,
        },
      },
    ]);
    return response.json(routesWithRes);
  } catch (error) {
    next(error);
  }
};

const getRoute = async (request: Request, response: Response, next: NextFunction) => {
  const { routeId } = request.params;
  try {
    const route = await RouteModel.findById(routeId);
    return response.json(route);
  } catch (error) {
    next(error);
  }
};

const putRoute = async (request: Request, response: Response, next: NextFunction) => {
  const { routeId } = request.params;
  try {
    const route = await RouteModel.findOneAndUpdate({ _id: routeId }, { ...request.body }, { new: true });
    return response.json(route);
  } catch (error) {
    next(error);
  }
};

const deleteRoute = async (request: Request, response: Response, next: NextFunction) => {
  const { routeId } = request.params;
  try {
    const route = await RouteModel.findById(routeId);
    if (route === null) {
      return response.status(404).json({ msg: `Route with id ${routeId} not found` });
    }
    await BookingModel.remove({ route: routeId });
    const result = await RouteModel.remove({ _id: routeId });
    return response.json(result);
  } catch (error) {
    next(error);
  }
};

export default {
  postRoute,
  getAllRoutes,
  findRoutes,
  getRoute,
  putRoute,
  deleteRoute,
  findRoutesWithDetails,
  validations,
};
