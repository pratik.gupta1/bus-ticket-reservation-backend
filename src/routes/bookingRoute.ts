import { Router } from "express";
import controller from "../controllers/bookingController";
import validateBody from "../middlewares/validateBody";

const router = Router();

router.route("/").post(validateBody(controller.validations.postBooking), controller.postBooking);

// TODO all auth to deleteBooking. Currently anyone can delete!
router.route("/:bookingId").get(controller.getBooking).delete(controller.deleteBooking);

export default router;
