import { Router } from "express";
import controller from "../controllers/adminController";
import authJwt from "../middlewares/authJwt";
import validateBody from "../middlewares/validateBody";

const router = Router();

router.route("/signin").post(validateBody(controller.validations.signin), controller.signin);
router.route("/verifyToken").get(authJwt.verifyToken, (request, response) => response.json({ msg: `success` }));

export default router;
