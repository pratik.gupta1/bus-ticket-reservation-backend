import { Router } from "express";
import controller from "../controllers/busController";
import validateBody from "../middlewares/validateBody";
import authJwt from "../middlewares/authJwt";

const router = Router();

router
  .route("/")
  .get(controller.getAllBuses)
  .post(authJwt.verifyToken, validateBody(controller.validations.postBus), controller.postBus);
router
  .route("/:busId")
  .get(controller.getBus)
  .delete(authJwt.verifyToken, controller.deleteBus)
  .put(authJwt.verifyToken, validateBody(controller.validations.putBus), controller.putBus);

export default router;
