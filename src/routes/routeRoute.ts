import { Router } from "express";
import controller from "../controllers/routeController";
import authJwt from "../middlewares/authJwt";
import validateBody from "../middlewares/validateBody";

const router = Router();

router
  .route("/")
  .get(controller.validations.getAllRoutes, controller.getAllRoutes)
  .post(authJwt.verifyToken, validateBody(controller.validations.postRoute), controller.postRoute);

router.route("/findRoutes").get(validateBody(controller.validations.findRoutes), controller.findRoutes);
router
  .route("/findRoutesWithDetails")
  .get(validateBody(controller.validations.findRoutesWithDetails), controller.findRoutesWithDetails);

router
  .route("/:routeId")
  .get(controller.getRoute)
  .put(authJwt.verifyToken, controller.putRoute)
  .delete(authJwt.verifyToken, controller.deleteRoute);

export default router;
