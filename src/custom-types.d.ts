declare global {
    namespace NodeJS {
      interface ProcessEnv {
        MONGO_CON_STRING: string;
        JWT_SECRET: string;
      }
    }
    namespace Express {
      interface Request {
      }
    }
  }
  
  // If this file has no import/export statements (i.e. is a script)
  // convert it into a module by adding an empty export statement.
  export {}