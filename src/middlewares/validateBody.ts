import { NextFunction, Request, Response } from "express";
import { ValidationChain, validationResult } from "express-validator";

const validateBody = (validators: ValidationChain[]) => {
  const bodyErrorHandler = async (request: Request, response: Response, next: NextFunction) => {
    const errors = validationResult(request);
    if (!errors.isEmpty()) {
      return response.status(422).json({ errors: errors.array() });
    }
    next();
  };
  return [...validators, bodyErrorHandler];
};

export default validateBody;
